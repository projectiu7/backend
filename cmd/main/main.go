package main

import (
	"log"

	"bitbucket.org/projectiu7/backend/src/master/internal/server"
	constants "bitbucket.org/projectiu7/backend/src/master/pkg/const"
)

func main() {
	app := server.NewApp()

	if err := app.Run(constants.Port); err != nil {
		log.Fatal(err)
	}
}
