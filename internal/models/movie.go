package models

// Movie структура фильма
type Movie struct {
	ID    string `json:"id"`
	Title string `json:"title"`
}
